import java.util.Scanner;

public class VirtualPetApp {
    
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);

        Jellyfish[] smack = new Jellyfish[1];

        for (int i = 0; i < smack.length; i++) {

            System.out.println("Jellyfish #" + i);
            System.out.println("");
            System.out.println("");
            System.out.println("Is it deadly? true if yes, false if no.");
            boolean d = reader.nextBoolean();
            reader.nextLine();

            System.out.println("What is the size (in cm)");
            int s = reader.nextInt();
            reader.nextLine();

            System.out.println("What region is it found in?");
            String r = reader.nextLine();
            System.out.println("");

            smack[i] = new Jellyfish(d, s, r);  //d for deadly, s for size, r for region
        }
		
		int lastAnimal = smack.length-1;
		
		System.out.println("Re-enter the region for the last animal");
		String newRegion = reader.nextLine();
		smack[lastAnimal].setRegion(newRegion);
	
        System.out.println(smack[lastAnimal].getDeadly());
		System.out.println(smack[lastAnimal].getBodySize());
		System.out.println(smack[lastAnimal].getRegion());
    }
}
