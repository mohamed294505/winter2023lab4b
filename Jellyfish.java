
public class Jellyfish{
    
    private boolean deadly;
    private double bodySize;
    private String region;
    
    public Jellyfish(boolean deadly, int bodySize, String region) {
        this.deadly = deadly;
        this.bodySize = bodySize;
        this.region = region;
    }
	
	public boolean getDeadly(){
		return this.deadly;
	}
	public double getBodySize(){
		return this.bodySize;
	}
	public String getRegion(){
		return this.region;
	}

	public void setRegion(String region){
		this.region = region;
	}
    
    public boolean isDeadly() {
        if (deadly) {
            return true;
        } else {
            return false;
        }
    }
    
    public String regionFound() {
        return "This jellyfish is located in " + region;
    }
}
